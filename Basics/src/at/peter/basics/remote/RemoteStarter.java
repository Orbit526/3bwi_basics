package at.peter.basics.remote;

public class RemoteStarter {

	public static void main(String[] args) {
		Battery b1 = new Battery(40, "AA");
		Battery b2 = new Battery(70, "AAA");
		
		Remote r1 = new Remote(300, 70, 30, 20, 10, false, b1);	
		Remote r2 = new Remote(400, 40, 40, 40, 40, false, b2);
		
		r1.turnOn();
		r2.turnOff();
		
		r1.sayHello();
		r2.sayHello();
	}

}
