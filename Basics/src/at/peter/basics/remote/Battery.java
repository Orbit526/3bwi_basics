package at.peter.basics.remote;

public class Battery {
	private int batteryStatus;
	private String type;

	public Battery(int batteryStatus, String type) {
		super();

		if (batteryStatus > 100) {
			batteryStatus = 100;
		} else if (batteryStatus < 0) {
			batteryStatus = 0;
		}

		this.batteryStatus = batteryStatus;
		this.type = type;
	}

	public int getBatteryStatus() {
		return batteryStatus;
	}

	public void setBatteryStatus(int batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	public String getType() {
		return type;
	}

}
