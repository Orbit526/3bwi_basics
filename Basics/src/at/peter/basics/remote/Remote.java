package at.peter.basics.remote;

public class Remote {

	private double weight;
	private double height;
	private double width;
	private double length;
	private int volume;
	private boolean turnedOn;
	
	private Battery battery;
	
	public Remote(double w, double h, double wi, double le, int v, boolean turn, Battery battery) {
		super();
		this.weight = w;
		this.height = h;
		this.width = wi;
		this.length = le;
		this.volume = v;
		this.turnedOn = turn;
		
		this.battery = battery;
	}

	public void turnOn()
	{
		System.out.println("I am turned on now!");
		this.turnedOn= true;
	}
	
	public void turnOff()
	{
		System.out.println("I am turned off now!");
		this.turnedOn = false;
	}
	
	public void sayHello()
	{
		System.out.println("Weight: " + this.weight + " Height: " + this.height + " Width: " + this.width + " Length: " + this.length + " Volume: " + this.volume + " TurnedOn: " + this.turnedOn + " Batterystatus: " +  battery.getBatteryStatus());
	}
	
}
