package at.peter.basics.car;

public class CarStarter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Producer p1 = new Producer("VW Polo", "Deutschland", 3); // name, country, discount (in percent)
		Engine e1 = new Engine("Diesel", 160, 7000000, 9.8); // type, power, kilometres, gasConsumption
		Car c1 = new Car("blue", 200, 25000, p1, e1); // color, maxSpeed, basicPrice

		Producer p2 = new Producer("Aston Martin DB11", "England", 6); // name, country, discount (in percent)
		Engine e2 = new Engine("Benzin", 510, 10000, 11); // type, power, kilometres, gasConsumption
		Car c2 = new Car("grey", 330, 190000, p2, e2); // color, maxSpeed, basicPrice

		Person pe1 = new Person("Rafael", "Oberhauser");

		pe1.addCar(c1);
		pe1.addCar(c2);

		c1.showProperties();
		c1.showManufacturer();
		c1.showEngine();

		c2.showProperties();
		c2.showManufacturer();
		c2.showEngine();

		pe1.showPerson();
	}
}
