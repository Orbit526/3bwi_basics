package at.peter.basics.car;

public class Engine {

	private String type;
	private int power;
	private int kilometres;
	private double gasConsumption;

	public Engine(String type, int power, int kilometres, double gasConsumption) {
		super();

		if (kilometres > 50000) {
			gasConsumption = gasConsumption * 1.098;
		}

		this.type = type;
		this.power = power;
		this.kilometres = kilometres;
		this.gasConsumption = gasConsumption;
	}

	public String getType() {
		return type;
	}

	public int getPower() {
		return power;
	}

	public double getGasConsumption() {
		return gasConsumption;
	}

	public void setGasConsumption(double gasConsumption) {
		this.gasConsumption = gasConsumption;
	}

	public int getKilometres() {
		return kilometres;
	}

	public void setKilometres(int kilometres) {
		this.kilometres = kilometres;
	}
}
