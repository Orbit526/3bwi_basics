package at.peter.basics.car;

public class Producer {
	private String name;
	private String country;
	private double disount;

	public Producer(String name, String country, double disount) {
		super();
		this.name = name;
		this.country = country;
		this.disount = disount;
	}

	public double getDisount() {
		return disount;
	}

	public void setDisount(double disount) {
		this.disount = disount;
	}

	public String getName() {
		return name;
	}

	public String getCountry() {
		return country;
	}

}
