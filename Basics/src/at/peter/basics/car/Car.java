package at.peter.basics.car;

public class Car {
	private String color;
	private int maxSpeed;
	private double basicPrice;
	private double realPrice;

	private Producer producer;
	private Engine engine;

	public Car(String color, int maxSpeed, double basicPrice, Producer producer, Engine engine) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basicPrice = basicPrice;
		this.producer = producer;
		this.engine = engine;

		realPrice = basicPrice - (basicPrice * producer.getDisount() / 100);
	}

	public void showProperties() {
		System.out.println("Farbe: " + this.color + " ; Topspeed: " + this.maxSpeed + " ; Basispreis: "
				+ this.basicPrice + " ; Verkaufspreis: " + realPrice);
	}

	public void showManufacturer() {
		System.out.println("Hersteller: " + producer.getName() + " ; Land: " + producer.getCountry() + " ; Rabatt: "
				+ producer.getDisount());
	}

	public void showEngine() {
		System.out.println("Type: " + engine.getType() + " ; Leistung: " + engine.getPower() + " ; Kilometres: "
				+ engine.getKilometres() + " ; Verbrauch: " + engine.getGasConsumption());
	}

}
