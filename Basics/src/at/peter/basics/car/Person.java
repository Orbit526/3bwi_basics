package at.peter.basics.car;

import java.util.ArrayList;

public class Person {

	private String firstName;
	private String lastName;
	private ArrayList<Car> cars;

	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.cars = new ArrayList<>();
	}

	public int getValueOfCars() {
		int carSum = 0;
		for (Car car : cars) {
			carSum++;
		}
		return carSum;
	}

	public void addCar(Car c) {
		this.cars.add(c);
	}

	public void showPerson() {
		System.out.println("Vorname: " + this.firstName + " ; Nachname: " + this.lastName + " ; AnzahlAutos: "
				+ getValueOfCars() + " ; Auto: " + this.cars);
	}

}
